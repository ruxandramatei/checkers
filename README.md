# Checkers Project

This is a school project written in **Python**, which implements the game of Checkers.

The purpose of this project was to understand and implement two important algorithms: **A star** and **Alpha-Beta**.

The game offers the user the opportunity to choose between the two algorithms, to set the difficulty of the game and to choose the way of playing: in console or on the board.

The board was implemented using **PyGame library**. The **UI** is simple and gives the user hints about the checkers that can be moved and available positions to move. The scope of these hints was to make the game easier to understand.

---


![](./img/checkers.png)


---

More information about the game cand be found here: https://en.wikipedia.org/wiki/English_draughts
