import sys
import time
from checkers import *
import pygame

# definim culorile

DIM_PATRATEL = 100
DIM_TABLA = 800
NR_PATRATELE = 8

def deseneaza_tabla(suprafata, stare):
    for x in range(0, DIM_TABLA, DIM_PATRATEL):
        for y in range(0, DIM_TABLA, DIM_PATRATEL):

            patratel = (x, y, DIM_PATRATEL, DIM_PATRATEL)
            
            i = int(x / DIM_PATRATEL)
            j = int(y / DIM_PATRATEL)

            alb = pygame.Color("#ffbf80")
            negru = pygame.Color( "#663300")

            if((i % 2 == 0 and j % 2 == 0) or (i % 2 == 1 and j % 2 == 1)):
                pygame.draw.rect(suprafata, alb, patratel)
            
            elif((i % 2 == 0 and j % 2 == 1) or (i % 2 == 1 and j % 2 == 0)):
                pygame.draw.rect(suprafata, negru, patratel)

def deseneaza_piese(suprafata, stare):
    for x in range(0, DIM_TABLA, DIM_PATRATEL):
        for y in range(0, DIM_TABLA, DIM_PATRATEL):
          
            i = int(x / DIM_PATRATEL)
            j = int(y / DIM_PATRATEL)

            alb = pygame.Color("#ffe6cc")
            negru = pygame.Color("#1a0d00")
            rege_alb = pygame.Color("#ff5500")
            rege_negru = pygame.Color("#991f00")
            offset = 50

            if(stare.tabla_joc.matr[i][j] == "a"):
                pygame.draw.circle(suprafata, alb, (y + offset, x + offset), 30)
                continue
            
            if(stare.tabla_joc.matr[i][j] == "n"):
                pygame.draw.circle(suprafata, negru, (y + offset, x + offset), 30)
                continue
            
            if(stare.tabla_joc.matr[i][j] == "A"):
                pygame.draw.circle(suprafata, rege_alb, (y + offset, x + offset), 30)
                continue
            
            if(stare.tabla_joc.matr[i][j] == "N"):
                pygame.draw.circle(suprafata, rege_negru, (y + offset, x + offset), 30)
                continue

def hint_mutare_user(suprafata, pozitii_valide):

    for pozitie_valida in pozitii_valide.keys():

            x, y = pozitie_valida[0], pozitie_valida[1]
            contur = 5

            conexiuni = [(y * DIM_PATRATEL, x * DIM_PATRATEL), 
            (y * DIM_PATRATEL + DIM_PATRATEL, x * DIM_PATRATEL),
            (y * DIM_PATRATEL + DIM_PATRATEL, x * DIM_PATRATEL + DIM_PATRATEL),
            (y * DIM_PATRATEL, x * DIM_PATRATEL + DIM_PATRATEL)]
            
            verde = pygame.Color("#80ff00")

            pygame.draw.lines(suprafata, verde, True, conexiuni, contur)

def hint_piesa_user(suprafata, pozitii_valide):

        for pozitie_valida in pozitii_valide.keys():

            x, y = pozitie_valida[0], pozitie_valida[1]
            contur = 5

            conexiuni = [(y * DIM_PATRATEL, x * DIM_PATRATEL), 
            (y * DIM_PATRATEL + DIM_PATRATEL, x * DIM_PATRATEL),
            (y * DIM_PATRATEL + DIM_PATRATEL, x * DIM_PATRATEL + DIM_PATRATEL),
            (y * DIM_PATRATEL, x * DIM_PATRATEL + DIM_PATRATEL)]
            
            albastru = pygame.Color("#00ffff")

            pygame.draw.lines(suprafata, albastru, True, conexiuni, contur)
            
def rulare(tabla_curenta, JMAX, JMIN, tip_algoritm, adancime):
    Joc.NR_COLOANE = 8 # ale tablei de joc
    Joc.NR_LINII = 8 # ale tablei de joc

    Joc.SIMBOLURI_JUC = ['a', 'n'] # simboluri jucatori

    Joc.JMIN = JMIN # jucatorul (utilizatorul)
    Joc.JMAX = JMAX # calculatorul

    Joc.GOL = '.' # simbolul de gol al tablei

    stare = Stare(tabla_curenta, Joc.SIMBOLURI_JUC[0], adancime) # jucatorul care incepe este alb
   
    pygame.init()
    # setez displayul
    pygame.display.set_caption("Joc  de dame")
    ecran = pygame.display.set_mode((800, 800))
    background = pygame.Surface((800, 800))
    # setez ceasul
    clock = pygame.time.Clock()

    # desenez tabla de joc
    deseneaza_tabla(background, stare)
    deseneaza_piese(background, stare)

    running = True

    piesa_apasata_x = -1
    piesa_apasata_y = -1
    target_x = -1
    target_y = -1
    culoare_piesa = ""
    posibilitati_piesa = {}  

    counter = 0 

    while running:

        deseneaza_tabla(background, stare)
        deseneaza_piese(background, stare)
        
        pozitii_sursa_valide = {}

        if(stare.j_curent == JMIN and ((piesa_apasata_x == -1 and piesa_apasata_y == -1) or ((piesa_apasata_x != -1 and piesa_apasata_y != -1) and target_x == -1 and target_y == -1))):
            # desenam posibilitatile de joc
            mutari_posibile = stare.tabla_joc.mutari_valide(stare.j_curent)
            hint_mutare_user(background, mutari_posibile)

            pozitii_sursa_valide = {}

            for pozitie_valida, array_directii in mutari_posibile.items():

                for deplasare in array_directii:
                    sursa = deplasare[0]

                    if(sursa not in pozitii_sursa_valide.keys()):
                        pozitii_sursa_valide[sursa] = [[pozitie_valida, deplasare[1]]]
                    else:
                        gasit = False

                        for tmp in pozitii_sursa_valide[sursa]:
                            if([pozitie_valida, deplasare[1]] == tmp):
                                gasit = True
                        if(gasit == False):
                            pozitii_sursa_valide[sursa].append([pozitie_valida, deplasare[1]])

            hint_piesa_user(background, pozitii_sursa_valide)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                # aici muta userul
                if afis_daca_final(stare, Joc.JMIN):

                    clock.tick(15)

                    running = False
                    
                    deseneaza_tabla(background, stare)
                    deseneaza_piese(background, stare)
                    
                    time.sleep(2)
                    exit()

                    break

                x, y = event.pos # preiau coordonatele event-ului
                x_tabla = int(x / DIM_PATRATEL)
                y_tabla = int(y / DIM_PATRATEL)

                # x si y nu se gasesc ca pozitii valide

                if(y_tabla, x_tabla) in pozitii_sursa_valide.keys():
                    # aici setez ce piesa se muta
                    piesa_apasata_x = y_tabla
                    piesa_apasata_y = x_tabla

                    culoare_piesa = stare.tabla_joc.matr[piesa_apasata_x][piesa_apasata_y]

                    for directie in pozitii_sursa_valide[(piesa_apasata_x, piesa_apasata_y)]:

                        if directie[0] not in posibilitati_piesa.keys():
                            posibilitati_piesa[directie[0]] = directie[1]
                        else:
                            posibilitati_piesa[directie[0]].append(directie[1])
                    continue
                
                elif (piesa_apasata_x != -1 and piesa_apasata_y != -1) and ((y_tabla, x_tabla) in posibilitati_piesa.keys()):
                    # aici trebuie sa fac o mutare
                    x_liber = y_tabla
                    y_liber = x_tabla

                    directii = posibilitati_piesa[(x_liber, y_liber)]
                    simbol_j = stare.tabla_joc.matr[piesa_apasata_x][piesa_apasata_y]
                    
                    update_matrice(stare.tabla_joc.matr, (x_liber, y_liber), directii, simbol_j) # updatez matricea

                    piesa_apasata_x = -1
                    piesa_apasata_y = -1
                    posibilitati_piesa = {}
                    culoare_piesa = {}

                    print(stare.j_curent)
                    print(stare.tabla_joc)

                    stare.j_curent = stare.jucator_opus() # schimb jucatorul

                    ###################################################
                    # aici joaca AI

                    if afis_daca_final(stare, JMIN):
                        
                        deseneaza_tabla(background, stare)
                        deseneaza_piese(background, stare)
                        
                        clock.tick(15)

                        running = False
                        
                        time.sleep(2)
                        exit()

                        break

                    if tip_algoritm == '1':
                        stare_actualizata = min_max(stare)
                    else:
                        stare_actualizata = alpha_beta(-5000, 5000, stare)

                    stare.tabla_joc = stare_actualizata.stare_aleasa.tabla_joc
                    
                    print(stare.j_curent)
                    print(stare.tabla_joc)

                    stare.j_curent = stare.jucator_opus() # schimb jucatorul

                    if afis_daca_final(stare, JMAX):
                        
                        deseneaza_tabla(background, stare)
                        deseneaza_piese(background, stare)

                        clock.tick(15)

                        running = False
                        
                        time.sleep(2)
                        exit()

                        break


        ecran.blit(background, (0, 0))

        pygame.display.flip()
        clock.tick(15)