import time
import copy
import sys

from userinterface import *

def parcurgere_DFS(jucator_opus, gol, matrice, punct_plecare, sens_venire_in_piesa_opusa, x_opus, y_opus, vector_vecini, pasi, pozitii_valide):
    are_deplasare = False

    piesa_anterioara_x = x_opus # retin coordonatele piesei anterioare (care AR TREBUI SA FIE  opusa)
    piesa_anterioara_y = y_opus 

    if(matrice[piesa_anterioara_x][piesa_anterioara_y].lower() == jucator_opus.lower()): # daca piesa mea anterioara (sarita) este opusa
        dir_deplasare = sens_venire_in_piesa_opusa

        next_x = piesa_anterioara_x + dir_deplasare[0] # ma duc la spatiu
        next_y = piesa_anterioara_y + dir_deplasare[1] # pe diagonala


        if (next_x < len(matrice) and next_x > -1) and (next_y > -1 and next_y < len(matrice[0])) and (matrice[next_x][next_y] == gol): # se incadreaza in linii si coloane si am gol
            # print(next_x, next_y)
            are_deplasare = True # piesa oponenta are o mutare dupa 

            pasi.append((dir_deplasare[0], dir_deplasare[1])) # pun directia de mutare in stiva pt ca mutarea e un pas in lant
            
            # aici stiu sigur ca as putea avea o deplasare
            poz = (next_x, next_y) # o pozitie posibil valida pt ca am gol, nu stiu sigur daca este capat de deplasare

            # caut o noua piesa oponenta peste care sa sar
            are_deplasare_mai_departe = False # folosita pt a vedea daca din golul curent eu pot sa mai adaug pasi

            for dir_urm in vector_vecini: # iau directia de deplasare
            
                piesa_urm_x = next_x + dir_urm[0] # iau piesa (oponenta sau nu) urmatoare
                piesa_urm_y = next_y + dir_urm[1] 

                if (piesa_urm_x > -1 and piesa_urm_x < len(matrice)) and (piesa_urm_y > -1 and piesa_urm_y < len(matrice[0])):
                    # print(piesa_urm_x, piesa_urm_y)
                    temp = are_deplasare_mai_departe
                    are_deplasare_mai_departe = parcurgere_DFS(jucator_opus, gol,matrice, punct_plecare, dir_urm, piesa_urm_x, piesa_urm_y, vector_vecini, pasi, pozitii_valide)
                    are_deplasare_mai_departe = temp or are_deplasare_mai_departe

            if(are_deplasare_mai_departe == False): # nu pot muta mai departe

                copie_stiva = copy.deepcopy(pasi)

                if poz not in pozitii_valide.keys(): # este prima pozitie valida gasita
                    pozitii_valide[poz] = [[punct_plecare, copie_stiva]]


                else:
                    
                    pozitii_valide[poz].append([punct_plecare, copie_stiva]) # adaug la lista de mutari posibile

            pasi.pop()
    return are_deplasare

class Joc:
    """
    Clasa care defineste jocul. Se va schimba de la un joc la altul.
    """

    NR_COLOANE = 8 # ale tablei de joc
    NR_LINII = 8 # ale tablei de joc

    SIMBOLURI_JUC = ['a', 'n'] # simboluri jucatori

    JMIN = None # jucatorul (utilizatorul)
    JMAX = None # calculatorul

    GOL = '.' # simbolul de gol al tablei

    # initializare tabla de joc
    def __init__(self, tabla=None):

        if tabla is not None:
            self.matr = tabla # daca se da o tabla preconfigurata o preiau

        else:
            self.matr = [[Joc.GOL for j in range(Joc.NR_COLOANE)] for i in range(Joc.NR_LINII)] # setez initial toata tabla cu gol

            for indexColoana in range(Joc.NR_COLOANE): # asez damele pe tabla dupa desen

                if indexColoana % 2 == 0: # daca indexul la coloana e par

                    self.matr[1][indexColoana] = 'a' # setez pe linia 1 o dama alba

                    self.matr[Joc.NR_LINII - 1][indexColoana] = 'n' # setez pe antepenultima
                    self.matr[Joc.NR_LINII - 3][indexColoana] = 'n' # si ultima linie dame negre

                else: # daca indexul este impar

                    self.matr[0][indexColoana] = 'a' # setez dame albe pe prima linie
                    self.matr[2][indexColoana] = 'a' # si pe a treia linie

                    self.matr[Joc.NR_LINII - 2][indexColoana] = 'n' # setez dama neagra pe penultima linie



    # Daca un jucator nu poate plasa nicio piesa, sau si-a pierdut toate piesele, pierde jocul, adversarul
    # devenind castigatorul
    """
        Metoda ce primeste ca parametru jucatorul actual
        si returneaza:
            - simbolul jucatorului castigator
            - remiza , in cazul in care scorul este egal
            - False, daca jocul continua
    """
    def final(self, jucator):
        # returnam simbolul jucatorului castigator 
        # sau returnam 'remiza'
        # sau 'False' daca nu s-a terminat jocul

        pozitii_valide = self.mutari_valide(jucator) # generez posibilitatile de mutare pentru jucatorul actual
        final = len(pozitii_valide) # inseamna ca nu am posibilitate de mutare

        if final == 0: # daca nu exista posibilitatea de mutare

            scor_jmin = self.punctaj_final(self.JMIN)   # numaram cate piese are fiecare jucator , indiferent daca sunt piese rege
            scor_jmax = self.punctaj_final(self.JMAX)   # sau nu

            if scor_jmax > scor_jmin:   # compar numarul de piese al fiecarui jucator
                return self.JMAX # JMAX a castigat, ii returnez simbolul

            elif scor_jmax == scor_jmin: # daca au acelasi scor
                return 'remiza' # am remiza

            else: # altfel
                return self.JMIN # JMIN a castigat, ii returnez simbolul

        return False # jocul inca continua

    """
        Metoda ce primeste ca parametru jucatorul actual si intoarce
        un dictionar cu mutarile valide pe care le are la dispozitie jucatorul
    """
    def mutari_valide(self, jucator):

        # exista 3 posibilitati de a te misca: de sus in jos(alb), de jos in sus(negru) si in toate directiile(rege)
        
        vecini_alb = [[1,-1], [1, 1]] # coordonatele de deplasare in jos pe diagonala, pentru piesa alba
        vecini_negru = [[-1, -1], [-1, 1]] # coordonatele de deplasare in sus pe diagonala, pentru piesa neagra
        vecini_alb_rege = [[-1, -1], [-1, 1]]
        vecini_negru_rege = [[1,-1], [1, 1]] 
        vecini_rege = [[-1, -1], [-1, 1], [1,-1], [1, 1]]

        # facem un dictionar in care:
        #          - cheile  = sunt pozitiile
        #          - valorile = lista cu punctul de start si stiva cu directiile spre pozitia valida
        # declar dictionarul cu pozitii valide

        pozitii_valide = {}

        if(jucator == self.JMAX):
            jucator_opus = self.JMIN
        else:
            jucator_opus = self.JMAX

        # parcurg toata tabla de joc si caut piese cu care sa joc    
        # adica de culoarea jucatorului actual (rege sau piesa normala)
        
        for x in range(self.NR_LINII):
            for y in range(self.NR_COLOANE):
                
                punct_plecare = (x, y)

                if self.matr[x][y].lower() == jucator.lower(): # am o piesa a jucatorului actual
                    
                    # setez deplasarea in functie de culoarea jucatorului actual
                    if self.matr[x][y] == 'a': # jucatorul este alb, si nu e rege, deci se deplaseaza in jos
                        vecini = vecini_alb

                    elif self.matr[x][y] == 'n': # jucatorul este negru si nu e rege
                        vecini = vecini_negru # deci se deplaseaza in sus
                        
                    elif self.matr[x][y] == 'A': # daca am rege se poate deplasa oriunde
                        vecini = vecini_rege
                    
                    elif self.matr[x][y] == 'N': # daca am rege se poate deplasa oriunde
                        vecini = vecini_rege

                    for poz_urm in vecini: # verific pozitiile disponibile de mutare
                        
                        next_x = x + poz_urm[0] # x-ul vecinului
                        next_y = y + poz_urm[1] # y-ul vecinului

                        # daca coordonatele nu intra in dimensiunile tablei
                        # sau zona e ocupata de o alta piesa de acceasi culoare
                        # trec mai departe
                        if  self.in_tabla(next_x, next_y) == False:
                            continue

                        if  self.matr[next_x][next_y].lower() == jucator.lower():
                            continue
                        

                        # Un jucator J poate pozitiona o piesa P fie cu o pozitie mai departe pe diagonala (in jos pentru alb si
                        # in sus pentru negru)

                        pasi = []

                        # acum, pozitia mea poate fi goala, caz in care pot muta pt ca e un capat de lant
                        if self.matr[next_x][next_y] == self.GOL:

                            poz = (next_x, next_y) # preiau pozitia actuala

                            pasi.append((poz_urm[0], poz_urm[1]))

                            copie_stiva = copy.deepcopy(pasi)

                            if poz not in pozitii_valide.keys(): # daca nu este deja pusa in dictionar, inseamna ca

                                pozitii_valide[poz] = [[punct_plecare, copie_stiva]] # o adaug impreuna cu indicii de pozitionare 

                            else:
                                gasit = False
                                for tmp in pozitii_valide[poz]:
                                    if([punct_plecare, copie_stiva] == tmp):
                                        gasit = True
                                if(gasit == False):
                                    pozitii_valide[poz].append([punct_plecare, copie_stiva]) # adaug la lista de mutari posibile

                            continue # continui sa caut la vecini cu for o mutare valida pentru piesa

                        # trebuie sa fac verificarea ca, daca am o piesa opusa
                        # aceasta trebuie sa aiba liber pe diagonala,

                        # Deci pozitiile admisibile ar fi pozitii libere pe diagonala, pe aceeasi directie de mutare
                        # dar la o distanta de 2 pozitii
                        
                        
                        # in pasi am o lista (o folosesc ca stiva) in care retin fiecare pas intermediar

                        # acum pasi = []
                        # eu in pasi vreau sa retin fiecare directie

                        piesa_anterioara_x = next_x # retin coordonatele piesei anterioare (care e opusa)
                        piesa_anterioara_y = next_y # pentru ca trebuie sa fie de culoare opusa
                                                    # aici stiu sigur ca am culoare opusa, din ifuri

                        if(self.in_tabla(piesa_anterioara_x + poz_urm[0], piesa_anterioara_y + poz_urm[1]) == False):
                            continue

                        if(self.matr[piesa_anterioara_x + poz_urm[0]][piesa_anterioara_y + poz_urm[1]] != self.GOL):
                            continue
                        
                        if(self.matr[x][y] == 'A'):
                            parcurgere_DFS(jucator_opus, self.GOL, self.matr, punct_plecare, poz_urm, piesa_anterioara_x, piesa_anterioara_y,vecini_alb_rege, pasi, pozitii_valide)
                        elif(self.matr[x][y] == 'N'):
                            parcurgere_DFS(jucator_opus, self.GOL, self.matr, punct_plecare, poz_urm, piesa_anterioara_x, piesa_anterioara_y,vecini_negru_rege, pasi, pozitii_valide)
                        else:
                            parcurgere_DFS(jucator_opus, self.GOL, self.matr, punct_plecare, poz_urm, piesa_anterioara_x, piesa_anterioara_y,vecini, pasi, pozitii_valide)
                    
        return pozitii_valide
        
    def in_tabla(self, x, y):
        return x in range(self.NR_LINII) and y  in range(self.NR_COLOANE)


    # intoarce o lista cu configuratiile posibile de mutare pentru un jucator
    def mutari(self, jucator):
        lista_mutari_posibile = []

        mutari_valide = self.mutari_valide(jucator)

        # cum eu plec de la pisea jucatorului
        #   1. pot sa am langa un gol, deci mutare directa
        #   2. merg din 2 in 2 pana la pozitia valida, pe directia data
        #      deci pot sa merg inapoi din 2 in doi inapoi , pana dau de piesa sursa

        # problema aceasta e rezolvata in metoda update_matrice

        for pozitie, modalitate_deplasare in mutari_valide.items():

            # intr-o pozitie valida poti ajunge de la mai multe puncte de start
            for tuplu_sursa_directii in modalitate_deplasare:
                
                # eu am un tuplu (sursa de unde se muta, directia pt a ajunge in cheia pozitie valida a dictionarului)
                directii = tuplu_sursa_directii[1] # pe 0 era pozitia sursa

                matrice_noua = copy.deepcopy(self.matr) # fac o copie a matricei actuale
                simbol_jucator = matrice_noua[tuplu_sursa_directii[0][0]][tuplu_sursa_directii[0][1]]

                update_matrice(matrice_noua, pozitie, directii, simbol_jucator) # fac mutarea , de la pozitie, merg pe directii cat pot de departe

                joc_nou = Joc(matrice_noua) # creez un joc

                lista_mutari_posibile.append(Joc(matrice_noua)) # adauga la lista de mutari posibile de joc 

        return lista_mutari_posibile # intorc lista de mutari posibile


    # intoarce cate piese are jucatorul primit ca marametru

    def punctaj_final(self, jucator):

        contor  = 0

        for linie in self.matr:
            contor = contor + linie.count(jucator.lower()) # numar cate piese are jucatorul (si litere mici si mari)
            contor = contor + linie.count(jucator.upper())

        return contor

    # EURISTICILE
    # atribui un scor pt o stare , in functie de cat de buna este pt jucator

    def euristica_1(self):
        # daca pe ultima linie pastrez piese, inseamna ca blochez aparitia unui rege care sa manance
        # intorc diferenta de numar de piese de pe ultima linie(prima linie si ultima de jos)
        
        puncte_jucator_jmin = 0
        puncte_jucator_jmax = 0

        if(Joc.JMIN.lower() == 'a'):
            puncte_jucator_jmin += self.matr[0].count(Joc.JMIN.lower()) + self.matr[0].count(Joc.JMIN.upper())
            puncte_jucator_jmax += self.matr[self.NR_LINII - 1].count(Joc.JMAX.lower()) + self.matr[self.NR_LINII - 1].count(Joc.JMAX.upper())
        else:
            puncte_jucator_jmin += self.matr[self.NR_LINII - 1].count(Joc.JMIN.lower()) + self.matr[self.NR_LINII - 1].count(Joc.JMIN.upper())
            puncte_jucator_jmax += self.matr[0].count(Joc.JMAX.lower()) + self.matr[0].count(Joc.JMAX.upper())

        # intorc diferenta dintre punctaje
        return puncte_jucator_jmax - puncte_jucator_jmin

    def euristica_2(self):
        # tinand cont de faptul ca odata ce o piesa avanseaza pe teritoriul opus
        # este mai aproape de a deveni rege, adica de a deveni o piesa imporrtanta
        # si de a manca

        puncte_jucator_jmin = self.punctaj_piesa_euristica_2(Joc.JMIN)
        puncte_jucator_jmax = self.punctaj_piesa_euristica_2(Joc.JMAX)

        # intorc diferenta dintre punctaje
        return puncte_jucator_jmax - puncte_jucator_jmin

    def punctaj_piesa_euristica_2(self, jucator):

        # cum un rege mananca mai mult, are un punctaj mai mare


        # punctaj piesa normala = 3
        # punctaj rege = 6

        # la acest punctaj adaugam indexul liniei (piesa normala)
        # sau numarul total de linii in cazul unui rege (el a parcurs tot)


        punctaj = 0

        if jucator.lower() == 'a':
            for index, linie in enumerate(self.matr):
                punctaj += (linie.count(jucator.upper()) * 6) * (Joc.NR_LINII + 1) + (linie.count(jucator.lower()) * 3) * (index + 1) 
        else:
            for index, linie in enumerate(self.matr):
                punctaj += (linie.count(jucator.upper()) * 6) * (Joc.NR_LINII + 1) + (linie.count(jucator.lower()) * 3) * (Joc.NR_LINII - index)      

        return punctaj

    def alege_euristica(self):
        return self.euristica_1()


    def afisare_punctaje_jucatori(self):
        print("Scor curent: ")

        if Joc.JMAX.lower() == 'a': # calculatorul joaca cu alb
            print(f"ALB: {self.punctaj_final(Joc.JMAX)}")
            print(f"NEGRU: {self.punctaj_final(Joc.JMIN)}")

        else: # Utilizatorul joaca cu alb
            print(f"ALB: {self.punctaj_final(Joc.JMIN)}")
            print(f"NEGRU: {self.punctaj_final(Joc.JMAX)}")


    def estimeaza_scor(self, adancime, jucator):

         t_final = self.final(jucator)

         if t_final == Joc.JMAX:
             return 999 + adancime
         elif t_final == Joc.JMIN:
             return -999 - adancime
         elif t_final == 'remiza':
             return 0
         else:
             return self.alege_euristica()

    def __str__(self):
        sir = '  '
        for nr_col in range(self.NR_COLOANE):
            sir += str(nr_col) + ' '
        sir += '\n'

        for linie in range(self.NR_LINII):
            sir += (str(linie) + ' ' + " ".join([str(i) for i in self.matr[linie]]) + "\n")
        return sir







"""
    Functie ce realizeaza o mutare:
        - matrice: matricea ce se va modifica
        - pozitie_valida: pozitia valida de mutare
        - directie_deplasare: directia cu care s-a ajuns in pozitia_valida
"""
def update_matrice(matrice, pozitie_valida, directii_prin_care_a_venit, jucator):

    x = pozitie_valida[0]
    y = pozitie_valida[1]
    
    if jucator.lower() == Joc.SIMBOLURI_JUC[0]: # daca sunt cu piesa alba

        if x == Joc.NR_LINII - 1: # si am ajuns pe ultima linie
            matrice[x][y] = jucator.upper() # fac piesa rege
        else:
            matrice[x][y] = jucator

    if jucator.lower() == Joc.SIMBOLURI_JUC[1]:# daca sunt cu piesa neagra

        if x == 0: # si am ajuns pe prima linie
            matrice[x][y] = jucator.upper() # fac piesa rege
        else:
            matrice[x][y] = jucator

    index_stiva = len(directii_prin_care_a_venit) - 1

    x = x - directii_prin_care_a_venit[index_stiva][0] # acum sunt pe vecinul imediat
    y = y - directii_prin_care_a_venit[index_stiva][1]

    # daca mutarea mea a fost facuta in adancime (poz anterioara nu e jucator)
    if not (matrice[x][y].lower() == jucator.lower()):
        piesa_urmatoare_x = x  # salvez index la piesa urmatoare de joc
        piesa_urmatoare_y = y

        matrice[piesa_urmatoare_x][piesa_urmatoare_y] = Joc.GOL # sterg piesa aflata pe traseu

        x = x - directii_prin_care_a_venit[index_stiva][0] # acum sunt pe urm pozitie libera
        y = y - directii_prin_care_a_venit[index_stiva][1]

        index_stiva = index_stiva - 1 # am facut un pas (salt peste o piesa oponenta)

        while matrice[x][y].lower() != jucator.lower(): # cat timp am spatiu gol
            piesa_urmatoare_x = x - directii_prin_care_a_venit[index_stiva][0]  # salvez index la piesa anterioara (urm in deplasarea inapoi)
            piesa_urmatoare_y = y - directii_prin_care_a_venit[index_stiva][1]

            matrice[piesa_urmatoare_x][piesa_urmatoare_y] = Joc.GOL # sterg piesa aflata pe traseu

            x = x - directii_prin_care_a_venit[index_stiva][0] - directii_prin_care_a_venit[index_stiva][0] # acum sunt pe urm pozitie libera
            y = y - directii_prin_care_a_venit[index_stiva][1] - directii_prin_care_a_venit[index_stiva][1]

            index_stiva = index_stiva - 1  # am facut un pas (salt peste o piesa oponenta)

    matrice[x][y] = Joc.GOL # am mutat piesa, deci nu mai este pe pozitia veche









class Stare:
    """
    Clasa folosita de algoritmii minimax si alpha-beta
    Are ca proprietate tabla de joc
    Functioneaza cu conditia ca in cadrul clasei Joc sa fie definiti JMIN si JMAX (cei doi jucatori posibili)
    De asemenea cere ca in clasa Joc sa fie definita si o metoda numita mutari() care ofera lista cu
    configuratiile posibile in urma mutarii unui jucator
    """

    ADANCIME_MAX = None

    def __init__(self, tabla_joc, j_curent, adancime, parinte=None, scor=None):
        self.tabla_joc = tabla_joc
        self.j_curent = j_curent

        # adancimea in arborele de stari
        self.adancime = adancime

        # scorul starii (daca e finala) sau al celei mai bune stari-fiice (pentru jucatorul curent)
        self.scor = scor

        # lista de mutari posibile din starea curenta
        self.mutari_posibile = []

        # cea mai buna mutare din lista de mutari posibile pentru jucatorul curent
        self.stare_aleasa = None

    def jucator_opus(self):
        if self.j_curent == Joc.JMIN:
            return Joc.JMAX
        else:
            return Joc.JMIN

    def mutari(self):
        l_mutari = self.tabla_joc.mutari(self.j_curent)
        juc_opus = self.jucator_opus()
        l_stari_mutari = [Stare(mutare, juc_opus, self.adancime - 1, parinte=self) for mutare in l_mutari]

        return l_stari_mutari

    def __str__(self):
        sir = str(self.tabla_joc) + "(Juc curent: " + self.j_curent + ")\n"
        return sir


""" Algoritmul MinMax """

def min_max(stare):
    if stare.adancime == 0 or stare.tabla_joc.final(stare.j_curent):
        stare.scor = stare.tabla_joc.estimeaza_scor(stare.adancime, stare.j_curent)
        return stare

    # calculez toate mutarile posibile din starea curenta
    stare.mutari_posibile = stare.mutari()
    # aplic algoritmul minimax pe toate mutarile posibile (calculand astfel subarborii lor)
    mutari_scor = [min_max(mutare) for mutare in stare.mutari_posibile]

    if stare.j_curent == Joc.JMAX:
        # daca jucatorul e JMAX aleg starea-fiica cu scorul maxim
        stare.stare_aleasa = max(mutari_scor, key=lambda x: x.scor)
    else:
        # daca jucatorul e JMIN aleg starea-fiica cu scorul minim
        stare.stare_aleasa = min(mutari_scor, key=lambda x: x.scor)

    stare.scor = stare.stare_aleasa.scor
    return stare


def alpha_beta(alpha, beta, stare):
    if stare.adancime == 0 or stare.tabla_joc.final(stare.j_curent):
        stare.scor = stare.tabla_joc.estimeaza_scor(stare.adancime, stare.j_curent)
        return stare

    if alpha >= beta:
        return stare  # este intr-un interval invalid deci nu o mai procesez

    stare.mutari_posibile = stare.mutari()

    if stare.j_curent == Joc.JMAX:
        scor_curent = float('-inf')

        for mutare in stare.mutari_posibile:
            # calculeaza scorul
            stare_noua = alpha_beta(alpha, beta, mutare)

            if scor_curent < stare_noua.scor:
                stare.stare_aleasa = stare_noua
                scor_curent = stare_noua.scor
            if alpha < stare_noua.scor:
                alpha = stare_noua.scor
                if alpha >= beta:
                    break

    elif stare.j_curent == Joc.JMIN:
        scor_curent = float('inf')

        for mutare in stare.mutari_posibile:
            stare_noua = alpha_beta(alpha, beta, mutare)

            if scor_curent > stare_noua.scor:
                stare.stare_aleasa = stare_noua
                scor_curent = stare_noua.scor

            if beta > stare_noua.scor:
                beta = stare_noua.scor
                if alpha >= beta:
                    break

    stare.scor = stare.stare_aleasa.scor

    return stare


def afis_daca_final(stare_curenta, jucator):

    final = stare_curenta.tabla_joc.final(jucator)

    if final:
        if final == "remiza":
            print("Remiza!")
        else:
            print("A castigat " + final)

        return True

    return False


def main():

    time_started = time.time() # momentul in care programul incepe sa ruleze

    nr_total_joc_jucator = 0 # numarul total de mutari pentru jucator
    nr_total_joc_calculator = 0 # numarul total de mutari pentru calculator

    # initializare algoritm
    raspuns_valid = False

    while not raspuns_valid:
        tip_algoritm = input("Algorimul folosit? (raspundeti cu 1 sau 2)\n 1.Minimax\n 2.Alpha-beta\n ")
        if tip_algoritm in ['1', '2']:
            raspuns_valid = True
        else:
            print("Nu ati ales o varianta corecta.")

    # initializare ADANCIME_MAX
    raspuns_valid = False
    while not raspuns_valid:
        n = input("Dificultate? (raspundeti cu 1, 2 sau 3)\n 1.Incepator\n 2.Mediu\n 3.Avansat\n ")
        if n in ['1', '2', '3']:
            Stare.ADANCIME_MAX = int(n) + 2 # adancimile le setez la 3, 4 si 5
            raspuns_valid = True
        else:
            print("Nu ati ales o varianta corecta.")

    # initializare tabla
    # temp = [['.', 'a', '.' ,'.' ,'.' ,'N', '.', 'a'],['a' ,'.', 'n', '.', 'a', '.', '.' ,'.'],['.' ,'.', '.' ,'.', '.', '.', '.', 'n'],['.', '.' ,'a' ,'.' ,'a' ,'.' ,'a' ,'.'],['.', 'a', '.', 'n' ,'.' ,'n' ,'.' ,'.'],['.', '.','n', '.' ,'.','.', '.', '.'],['.' ,'.' ,'.' ,'n' ,'.' ,'n', '.', 'n'],['n', '.', 'n', '.', 'n', '.', 'A', '.']]
    # temp = [['.', 'A', '.' ,'.' ,'.' ,'N', '.', 'A'],['A' ,'.', 'N', '.', 'A', '.', '.' ,'.'],['.' ,'.', '.' ,'.', '.', '.', '.', 'N'],['.', '.' ,'A' ,'.' ,'A' ,'.' ,'A' ,'.'],['.', 'A', '.', 'N' ,'.' ,'N' ,'.' ,'.'],['.', '.','N', '.' ,'.','.', '.', '.'],['.' ,'.' ,'.' ,'N' ,'.' ,'N', '.', 'N'],['N', '.', 'N', '.', 'N', '.', 'A', '.']]
    tabla_curenta = Joc()
    print("Tabla initiala")
    print(str(tabla_curenta))

    mod_joc = 1

    raspuns_valid = False
    while not raspuns_valid:
        n = input("Display? (raspundeti cu 1 sau 2)\n 1.GUI\n 2.Consola\n ")
        if n in ['1', '2']:
            mod_joc = int(n)
            raspuns_valid = True
        else:
            print("Nu ati ales o varianta corecta.")

    if(mod_joc == 1):

        Joc.JMAX = 'n'
        Joc.JMIN = 'a'

        copie_tabla = copy.deepcopy(tabla_curenta)
        rulare(copie_tabla, Joc.JMAX, Joc.JMIN, tip_algoritm, Stare.ADANCIME_MAX)

    else:

        # initializare jucatori
        [s1, s2] = Joc.SIMBOLURI_JUC.copy()  # lista de simboluri posibile
        raspuns_valid = False
        while not raspuns_valid:
            Joc.JMIN = str(input("Doriti sa jucati cu {} sau cu {}? ".format(s1, s2)))
            if Joc.JMIN in Joc.SIMBOLURI_JUC:
                raspuns_valid = True
            else:
                print("Raspunsul trebuie sa fie {} sau {}.".format(s1, s2))
        Joc.JMAX = s1 if Joc.JMIN == s2 else s2

        numar_mutari_user = 0
        numar_mutari_ai = 0

        linie = -1
        coloana = -1

        # creare stare initiala
        stare_curenta = Stare(tabla_curenta, Joc.SIMBOLURI_JUC[1], Stare.ADANCIME_MAX) # jucatorul care incepe este negru

        while True:
            if stare_curenta.j_curent == Joc.JMIN: # jucatorul este utilizatorul

                if afis_daca_final(stare_curenta, Joc.JMIN):
                    print("Culoarea {} a mutat {} ture\n".format(Joc.JMIN, numar_mutari_user))
                    print("Culoarea {} a mutat {} ture\n".format(Joc.JMAX, numar_mutari_ai))
                    break

                raspuns_valid = False

                while not raspuns_valid:
                    try:
                        pozitii_valide = stare_curenta.tabla_joc.mutari_valide(Joc.JMIN)

                        raspuns_despre_status_joc_valid = False

                        while raspuns_despre_status_joc_valid == False:
                            print("Continui jocul? (raspundeti cu 1 sau 2)\n 1.Continua\n 2.Exit\n ")
                            n = input("Optiune: ")

                            if n in ['1', '2']:
                                raspuns_despre_status_joc_valid = True

                                if n == '2': # optiunea de exit trebuie sa afiseze scorul curent
                                    stare_curenta.tabla_joc.afisare_punctaje_jucatori()

                                    print("Culoarea {} a mutat {} ture\n".format(Joc.JMIN, numar_mutari_user))
                                    print("Culoarea {} a mutat {} ture\n".format(Joc.JMAX, numar_mutari_ai))


                                    print("Timp executie:" + str(time.time() - time_started))
                                    sys.exit(0) # opresc programul
                            else:
                                print("Nu ati ales o varianta corecta.")

                        # Alegere pozitie libera unde vrei sa muti
                        raspuns_alegere_pozitie_libera = False

                        while raspuns_alegere_pozitie_libera == False:
                            print("Urmatoarele pozitii sunt valide: ")

                            for pozitie in pozitii_valide.keys():
                                print(pozitie, end=' ')

                            print('\n')
                            print("Alege mutare:")
                            # ok eu trebuie sa ii dau sa aleaga de la ce piesa trebuie sa vina mutarea # TO DO
                            linie = int(input("linie = "))
                            coloana = int(input("coloana = "))

                            if (linie, coloana) in pozitii_valide.keys():
                                raspuns_valid = True
                                raspuns_alegere_pozitie_libera = True
                            else:
                                print("Date de intrare invalide!")
                                raspuns_alegere_pozitie_libera = False
                                print('\n')


                        # Alegere de unde vrei sa muti pt ca poti ajunge intr-o zona valida din mai multe surse
                        raspuns_alegere_pozitie_sursa = False
                        pozitie_libera = (linie, coloana)

                        while raspuns_alegere_pozitie_sursa == False:
                            print("Urmatoarele pozitii ajung in pozitia libera dorita: ")
                            surse = {} # in surse retin toate punctele de start (cheie) -> [directie] care ajung in pozitia valida

                            for sursa in pozitii_valide[pozitie_libera]:
                                print(sursa[0], end=' ')
                                surse[sursa[0]] = sursa[1] # preiau stiva cu pasii parcursi de la sursa spre pozitia valida

                            # in surse ai punctele de start ca s cheie
                            # in valoare ai directia (imi trebuie la update matrice)

                            print('\n')
                            print("Alege punct sursa:")

                            # ok eu trebuie sa ii dau sa aleaga de la ce piesa trebuie sa vina mutarea

                            linie_sursa = int(input("linie sursa = "))
                            coloana_sursa = int(input("coloana sursa = "))
                            simbol_jucator = stare_curenta.tabla_joc.matr[linie_sursa][coloana_sursa]

                            if (linie_sursa, coloana_sursa) in surse:
                                
                                directii = surse[(linie_sursa, coloana_sursa)]

                                raspuns_valid = True
                                raspuns_alegere_pozitie_sursa = True
                            else:
                                print("Date de intrare invalide!")
                                raspuns_alegere_pozitie_sursa = False
                                print('\n')

                    except ValueError:
                        print("Coloana trebuie sa fie un numar intreg.")

                timp_start_gandire = time.time()

                pozitie_actuala = (linie, coloana)

                update_matrice(stare_curenta.tabla_joc.matr, pozitie_actuala, directii, simbol_jucator) # updatez matricea

                print("Timp gandire jucator: " + str(time.time() - timp_start_gandire) + " secunde\n")

                print("\nTabla dupa mutarea jucatorului")
                print(str(stare_curenta))

                numar_mutari_user += 1

                stare_curenta.j_curent = stare_curenta.jucator_opus() # schimb jucatorul

            # --------------------------------
            else:  # jucatorul e JMAX (calculatorul)

                if afis_daca_final(stare_curenta, Joc.JMAX):
                    print("Culoarea {} a mutat {} ture\n".format(Joc.JMIN, numar_mutari_user))
                    print("Culoarea {} a mutat {} ture\n".format(Joc.JMAX, numar_mutari_ai))
                    break

                # preiau timpul in milisecunde de dinainte de mutare
                t_inainte = int(round(time.time() * 1000))

                if tip_algoritm == '1':
                    stare_actualizata = min_max(stare_curenta)
                else:
                    stare_actualizata = alpha_beta(-5000, 5000, stare_curenta)


                timp_start_gandire = time.time()

                stare_curenta.tabla_joc = stare_actualizata.stare_aleasa.tabla_joc

                print("Timp gandire calculator: " + str(time.time() - timp_start_gandire) + " secunde\n")


                print("Tabla dupa mutarea calculatorului")
                print(str(stare_curenta))

                # S-a realizat o mutare. Schimb jucatorul cu cel opus
                stare_curenta.j_curent = stare_curenta.jucator_opus()
                numar_mutari_ai += 1
        print("Timp executie:" + str(time.time() - time_started))

    
if __name__ == "__main__":
    main()